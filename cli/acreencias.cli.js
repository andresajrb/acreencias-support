/**
 * @author A. Reyes
 * @description CLI de acreencias
 */
const _util = require('../utils/index.util');

const ctrlAcreencias = require('../controllers/acreencias.ctrl');

module.exports = {
  /**
   *
   * @param {Id de acreencias} id
   */
  async findAcreencia(id, country) {
    _util.clear();
    _util.setBigText('Acreencias');
    _util.startLoading('Buscando acreencia');

    try {
      let acr = await ctrlAcreencias.find(id, country);
      if (acr.length > 0) {
        _util.stopLoading('Acreencia encontrada', 'success');
        console.table(acr);

        if (acr[0].COINCIDEN == false) {
          let isUpdate = await _util.askQuestions(
            'a',
            'confirm',
            'Las acreencias no coinciden ¿Desea actualizar?'
          );
          if (isUpdate.a) {
            _util.startLoading('Actualizando acreencia');
            await ctrlAcreencias.update(id, country);
            _util.stopLoading('Acreencia actualizada', 'success');
          }
        }
      } else {
        _util.stopLoading('No se encontro registros', 'fail');
      }
    } catch (error) {
      console.log(error);
      _util.stopLoading(
        'Falla interna, por favor notificar a Caja Registradora',
        'fail'
      );
    }
  }
};
