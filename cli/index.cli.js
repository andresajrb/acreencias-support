/**
 * @author A. Reyes
 * @description Pide más datos y reedirigi segun el tipo de operación
 */

//Util
const _util = require('../utils/index.util');

//CLI
const cliAcreencia = require('./acreencias.cli')

async function main(type) {
    if (type.toLowerCase() == 'acreencias') {
        let askCountry = await _util.askQuestions(
            'country',
            'list',
            'Seleccione el país: ', ['VE', 'CR', 'GT', 'SV']
        );
        let isAcreenciaNumber = false;
        let askAcreencia = null;
        while (!isAcreenciaNumber) {
            askAcreencia = await _util.askQuestions(
                'id',
                'number',
                'Escriba el numero de acreencia: '
            );
            if (Number.isInteger(askAcreencia.id)) {
                isAcreenciaNumber = true
            } else {
                _util.setInfo('Ingrese correctamente el id de acreencia.. ', 'error')
            }
        }
        await cliAcreencia.findAcreencia(askAcreencia.id, askCountry.country)
    }
}

module.exports = {
    main
};