/**
 * @author Cesar Ramirez
 */

const configJt400 = require('../config/jt400/jt400.config');
const queryUtils = require('../utils/queries.util');

module.exports = class creditService {
  constructor() {
    this.classJt400 = new configJt400();
  }

  async getBalanceCreditById(clientId, country, store, pos) {
    try {
      return await this.classJt400.getBalanceCreditById(
        clientId,
        country,
        store,
        pos
      );
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  isCreditBalanceValid(SUMA_MOVIMIENTOS, SALDO_CAJA, SALDO_MENUEPA) {
    return this.areAllEqual(SUMA_MOVIMIENTOS, SALDO_CAJA, SALDO_MENUEPA);
  }

  async updateBalance(clientId, country, store, pos) {
    let creditBalanceInfo = await this.getBalanceCreditById(
      clientId,
      country,
      store,
      pos
    );

    if (creditBalanceInfo.length > 0) {
      let {
        SALDO_CAJA,
        SUMA_MOVIMIENTOS,
        SALDO_MENUEPA,
        ACREENCIA
      } = creditBalanceInfo[0];

      if (
        !this.isCreditBalanceValid(SUMA_MOVIMIENTOS, SALDO_CAJA, SALDO_MENUEPA)
      ) {
        await this.updateBalanceCredit(
          ACREENCIA,
          parseFloat(SUMA_MOVIMIENTOS),
          queryUtils.updateCreditBalance,
          country,
          store,
          pos
        );
        await this.updateBalanceCredit(
          ACREENCIA,
          parseFloat(SUMA_MOVIMIENTOS),
          queryUtils.updateCreditBalanceVentas,
          country,
          store,
          pos
        );
      }

      return true;
    }

    return false;
  }

  async updateBalanceCredit(creditId, amount, query, country, store, pos) {
    await this.classJt400.updateBalanceCredit(
      creditId,
      amount,
      query,
      country,
      store,
      pos
    );
  }

  async getCreditBalanceInfoComplete(clientId, country, store, pos) {
    let creditBalanceInfo = await this.getBalanceCreditById(
      clientId,
      country,
      store,
      pos
    );

    if (creditBalanceInfo.length > 0) {
      let {
        SALDO_CAJA,
        SUMA_MOVIMIENTOS,
        SALDO_MENUEPA
      } = creditBalanceInfo[0];

      this.areAllEqual(SALDO_CAJA, SUMA_MOVIMIENTOS, SALDO_MENUEPA)
        ? (creditBalanceInfo[0].COINCIDEN = true)
        : (creditBalanceInfo[0].COINCIDEN = false);

      return creditBalanceInfo;
    }

    return [];
  }

  areAllEqual(...values) {
    let checkValue = values[0];
    for (let i = 1; i < values.length; i++) {
      if (values[i] != checkValue) return false;
    }

    return true;
  }
};
