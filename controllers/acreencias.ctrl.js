/**
 * @author A. Reyes | Cesar Ramirez | J. Daza
 * @description Controlador para las peteiciones de las operaciones de acreencias
 */

const ServiceAcreencias = require('../services/acreencias.srv');

/**
 * @description Actualiza la acreencia dada
 * @param {ID de acreencia} idAcreencia
 * @param {Pais} country
 * @param {Tienda} store
 * @param {Caja} pos
 */
async function update(idAcreencia, country, store = 1, pos = 0) {
  try {
    let serviceAcreencias = new ServiceAcreencias();
    let response = await serviceAcreencias.updateBalance(
      idAcreencia,
      country,
      store,
      pos
    );
    return {
      status: response ? 'success' : 'fail',
      message: 'Acrencia actualizada'
    };
  } catch (error) {
    console.log(error);
    return error;
  }
}

/**
 * @description Busca la acreencia dada
 * @param {*} idAcreencia
 * @param {*} country
 * @param {*} store
 * @param {*} pos
 */
async function find(idAcreencia, country, store = 1, pos = 0) {
  try {
    let serviceAcreencias = new ServiceAcreencias();
    let acreencia = await serviceAcreencias.getCreditBalanceInfoComplete(
      idAcreencia,
      country,
      store,
      pos
    );
    return acreencia;
  } catch (error) {
    console.log(error);
    return error;
  }
}

module.exports = {
  update,
  find
};
