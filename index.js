/**
 * @author J. Daza | A. Reyes//Ingrese su nombre si trabajo en este proyecto
 * @description
 */

//Util
const _util = require('./utils/index.util');

//CLI
const _cli = require('./cli/index.cli');

async function main() {
    _util.clear();
    _util.setBigText('CRSUPPORT');
    let askProcess = await _util.askQuestions(
        'type',
        'list',
        'Seleccione el tipo de operación: ', ['Acreencias', 'Transacciones']
    );
    await _cli.main(askProcess.type);
}

main();