module.exports = {
    CR: [
        {
            store: 1,
            ip: '192.168.118.109',
            dns: 'epacr.epa.com',
            user: 'USERDB2',
            password: 'USERDB2'
        },
        {
            store: 2,
            ip: '10.17.16.11',
            dns: 'epacr2.epa.com',
            user: 'USERDB2',
            password: 'USERDB2'
        },
        {
            store: 3,
            ip: '10.17.24.11',
            dns: 'epacr3.epa.com',
            user: 'USERDB2',
            password: 'USERDB2'
        },
        {
            store: 4,
            ip: '10.17.32.11',
            dns: 'epacr4.epa.com',
            user: 'USERDB2',
            password: 'USERDB2'
        },
        {
            store: 5,
            ip: '10.17.41.11',
            dns: 'epacr5.epa.com',
            user: 'USERDB2',
            password: 'USERDB2'
        },
        {
            store: 6,
            ip: '10.17.49.11',
            dns: 'epacr6.epa.com',
            user: 'USERDB2',
            password: 'USERDB2'
        }
    ],
    /**
     * TODO
     * No se prueba por ahora
     */
    GT: [
        {
            store: 1,
            ip: '10.48.11.24',
            dns: 'epagt.epa.com'
        },
        {
            store: 2,
            ip: '10.19.17.11',
            dns: 'epagt2.epa.com'
        },
        {
            store: 3,
            ip: '10.19.25.11',
            dns: 'epagt3.epa.com'
        },
        {
            store: 4,
            ip: '10.19.41.11',
            dns: 'epagt4.epa.com'
        },
    ],
    SV: [
        {
            store: 1,
            ip: '10.48.11.22',
            dns: 'epasv.epa.com'
        },
        {
            store: 2,
            ip: '10.18.17.11',
            dns: 'epasv2.epa.com'
        },
        {
            store: 3,
            ip: '10.18.25.11',
            dns: 'epasv3.epa.com'
        },
    ],
    VE: [
        {
            store: 1,
            ip: '10.48.11.10',
            dns: 'epa.epa.com'
        },
        {
            store: 2,
            ip: '10.1.25.11',
            dns: 'epa2.epa.com'
        },
        {
            store: 3,
            ip: '10.1.17.11',
            dns: 'epa3.epa.com'
        },
        {
            store: 4,
            ip: '10.4.25.11',
            dns: 'epa4.epa.com'
        },
        {
            store: 5,
            ip: '10.4.33.11',
            dns: 'epa5.epa.com'
        },
        {
            store: 6,
            ip: '10.2.9.11',
            dns: 'epa6.epa.com'
        },
        {
            store: 7,
            ip: '10.1.33.11',
            dns: 'epa7.epa.com'
        },
        {
            store: 8,
            ip: '10.2.17.11',
            dns: 'epa8.epa.com'
        },
        {
            store: 9,
            ip: '10.3.9.11',
            dns: 'epa9.epa.com'
        },
        {
            store: 10,
            ip: '10.2.25.11',
            dns: 'epa10.epa.com'
        },
        {
            store: 11,
            ip: '10.4.9.11',
            dns: 'epa11.epa.com'
        },
        {
            store: 12,
            ip: '10.2.33.11',
            dns: 'epa12.epa.com'
        },
        {
            store: 13,
            ip: '10.2.41.11',
            dns: 'epa13.epa.com'
        },
        {
            store: 14,
            ip: '10.4.17.11',
            dns: 'epa14.epa.com'
        },
        {
            store: 15,
            ip: '10.1.26.11',
            dns: 'epa15.epa.com'
        },
        {
            store: 16,
            ip: '10.3.17.11',
            dns: 'epa16.epa.com'
        },
        {
            store: 17,
            ip: '10.4.41.11',
            dns: 'epa17.epa.com'
        }
    ]
}
