/**
 * @author J. Daza | A. Reyes
 * @description Tiene las utilidades del CLI
 */

//PLugins
const _inquirer = require('inquirer');
const _ora = require('ora');
const _clear = require('clear');
const _cfonts = require('cfonts');
const _colors = require('colors');

//Temporal loading
let oraTemp = null;

/**
 * 
 * @param {Nombre del key que retornara} name 
 * @param {Tipo de pregunta [Consulta documentación de inquirer npm]} type 
 * @param {Mensaje que se mostrara en pantalla} message 
 * @param {Lista de opciones si es type: list | Por defecto es [null]} items 
 */
const askQuestions = async(name, type, message, items = null) => {
    let question = {
        name,
        type,
        message
    };
    if (items != null) {
        question.choices = items;
    }
    return await _inquirer.prompt(question)
};

/**
 * 
 * @param {Texto a visualizar} text 
 */
const startLoading = async text => {
    oraTemp = _ora(text);
    oraTemp.spinner = 'dots4' //'dots4' es el spinner, leer la doc para más spinners
    oraTemp.start()
};

/**
 * 
 * @param {Texto a visualizar} text 
 * @param {Tipo de loading, por defecto el normal} type 
 */
const stopLoading = async(text = 'Finalizado', type = 'stop') => {

    if (type == 'success') {
        oraTemp.succeed(text);
    }
    if (type == 'warning') {
        oraTemp.warn(text);
    }
    if (type == 'info') {
        oraTemp.info(text);
    }
    if (type == 'fail') {
        oraTemp.fail(text);
    }
    if (type == 'stop') {
        oraTemp.stop();
    }


};

const clear = async() => {
    _clear();
};

/**
 * 
 * @param {Texto a visualizar} text 
 * @param {Estilo de fuente} font 
 * @param {Alineacion} align 
 */
const setBigText = async(text, font = 'chrome', align = 'left') => {
    _cfonts.say(text, {
        font,
        align,
        colors: ['cyan', '#f80'],
        background: 'transparent',
        letterSpacing: 1,
        lineHeight: 1,
        space: true,
        maxLength: '0'
    });
};

const setInfo = async(text = '', type = 'normal') => {
    if (type == 'error') {
        console.log(text.bold.red)
    }
    if (type == 'normal') {
        console.log(text.bold.white)
    }
}

module.exports = {
    askQuestions,
    startLoading,
    stopLoading,
    clear,
    setBigText,
    setInfo
};