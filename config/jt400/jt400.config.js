/**
 * @author Cesar Ramirez
 * @description Configuracion de la conexión a la base de datos DB2
 */

const jt400 = require('node-jt400');
const queryUtils = require('../../utils/queries.util');
const ip = require('../../utils/ip.util');

module.exports = class Jt400 {
  constructor() {}

  async getBalanceCreditById(clientId, country, store, pos) {
    try {
      let connection = await jt400.pool(
        this.getConnection(country, store, pos)
      );
      return await connection.query(queryUtils.finBalanceCreditById, [
        clientId
      ]);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async updateBalanceCredit(creditId, amount, query, country, store, pos) {
    return new Promise(async (resolve, reject) => {
      let connection = await jt400.pool(
        this.getConnection(country, store, pos)
      );

      connection
        .update(query, [amount, parseInt(creditId)])
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  getConnection(country, store, pos) {
    return this.getHost(ip[country], store, pos);
  }

  getHost(country, store, pos) {
    let config = {
      host: null,
      user: null,
      password: null
    };

    if (pos == 0)
      for (let item of country) {
        if (item.store == store) {
          config.host = item.ip;
          config.user = item.user;
          config.password = item.password;
        }
      }
    return config;
  }
};
